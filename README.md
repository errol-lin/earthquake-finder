# Earthquake Finder

## Architecture

The app is built using a simplified **CLEAN** architecture that defines the *Separation of Concerns* where the code base is separated into 3 layers, which are **Presentation**, **Domain** and **Data**. Each layer has its own responsibility and can only talk to the layer below it through protocol, so they are independent of each other. This makes the app easier to maintain and test, and we can even replace one of the layers with something else, without affecting other layers.

- Data Layer: The lowest layer in the project. It's generic and not aware of any business or presentation logic. Responsible for making network request, reading/writing to the database, etc.

- Domain Layer: The middle layer in the project. It communicates with **Data** layer through a `Repository` protocol. This layer is responsible for business/domain logic of the app.

- Presentation Layer: The highest layer in the project. It communicates with **Domain** layer through a `UseCase` protocol, which encapsulates a single and specific task that can be performed. This layer is responsible for preparing/formatting data to display in UI.

## Design Pattern

In **Presentation** layer, **MVP** design pattern is used to split out any logic from the ViewController, and move the view logic into a `Presenter` class. This makes ViewController a dumb UI class with minimum logic. 

The `Presenter` is responsible for preparing data and formatting it to a ready-for-display state, and passing that to the view through `Display` protocol. The view class conforms to the `Display` protocol, and the view should be dumb enough that it only assigns the value to some UI components(like assigning the value to a label). The `Presenter` should be generic and UI independent(it should not have access to `UIKit`), it doesn't know anything about the view class or how the data will be displayed in the view.

## How To Run

The app is developed using **Xcode 10.1** and **Swift 4.2.1**, and uses **Carthage 0.31.0** as the dependency manager, please make sure you have those tools installed.

### Steps
1. Clone the project to your Mac.
2. In terminal, `cd` to the root directory of the project.
3. In terminal, run `carthage bootstrap --platform iOS --no-use-binaries`.
4. Open the project in **Xcode 10.1** and run on **iPhone Simulator**.

## Frameworks

Third-party frameworks have been used in the project to help development.

|Framework              |Version            |
|---|---|
|[RxSwift](https://github.com/ReactiveX/RxSwift)      |4.3.1   |
|[Swinject](https://github.com/Swinject/Swinject)       |2.5.0   |   |
|[SwinjectStoryboard](https://github.com/Swinject/SwinjectStoryboard)       |2.1.0   |   |