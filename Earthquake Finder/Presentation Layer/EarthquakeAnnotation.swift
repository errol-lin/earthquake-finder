//
//  EarthquakeAnnotation.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import MapKit

final class EarthquakeAnnotation: NSObject, MKAnnotation {
    
    let title: String?
    
    let subtitle: String?
    
    let coordinate: CLLocationCoordinate2D
    
    private let longitude: Double
    
    private let latitude: Double
    
    init(longitude: Double, latitude: Double, title: String?, subtitle: String?) {
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.title = title
        self.subtitle = subtitle
        self.longitude = longitude
        self.latitude = latitude
        super.init()
    }
}
