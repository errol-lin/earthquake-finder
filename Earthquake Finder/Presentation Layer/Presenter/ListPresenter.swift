//
//  ListPresenter.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import RxSwift

protocol ListPresenting: class {
    func viewDidBecomeReady()
    func fetchList()
    func prepareData(for presenter: MapDetailPresenting)
    func prepareData(for presenter: MapListPresenting)
    func didSelectEarthquake(index: Int)
    func didSelectList()
}

protocol ListDisplay: class {
    func setMapButton(title: String)
    func insert(earthquakeList: [String])
    func hideRefreshIndicator()
}

protocol ListRouter: class {
    func navigate(withIdentifier identifier: String)
}

final class ListPresenter {

    // MARK: - Properties
    
    private unowned let display: ListDisplay
    
    private unowned let router: ListRouter
    
    private let earthquakeInfoUseCase: FetchEarthquakeInfoUseCase
    
    private let disposeBag: DisposeBag = .init()
    
    private var selectedEarthquake: Earthquake?
    
    private var earthquakes: [Earthquake] = []
    
    // MARK: - Initialiser
    
    init(display: ListDisplay, router: ListRouter, earthquakeInfoUseCase: FetchEarthquakeInfoUseCase) {
        self.display = display
        self.router = router
        self.earthquakeInfoUseCase = earthquakeInfoUseCase
    }
}

extension ListPresenter: ListPresenting {
    
    func viewDidBecomeReady() {
        display.setMapButton(title: "View in map")
    }
    
    func fetchList() {
        earthquakeInfoUseCase.fetchInfo().subscribe(
            onSuccess: { [weak self] (info: EarthquakeInfo) in
                guard let strongSelf = self else { return }
                strongSelf.display.hideRefreshIndicator()
                strongSelf.display.insert(earthquakeList: info.earthquakes.map { $0.place })
                strongSelf.earthquakes = info.earthquakes
            },
            
            onError: { (error: Error) in
                // Handle error
                print(error.localizedDescription)
            }
        ).disposed(by: disposeBag)
    }
    
    func didSelectEarthquake(index: Int) {
        selectedEarthquake = earthquakes[index]
        router.navigate(withIdentifier: "ShowMapDetail")
    }
    
    func didSelectList() {
        router.navigate(withIdentifier: "ShowMapList")
    }
    
    func prepareData(for presenter: MapDetailPresenting) {
        presenter.earthQuake = selectedEarthquake
    }
    
    func prepareData(for presenter: MapListPresenting) {
        presenter.earthQuakes = earthquakes
    }
}
