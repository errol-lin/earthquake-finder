//
//  MapDetailPresenter.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation

protocol MapDetailPresenting: class {
    func viewDidBecomeReady()
    var earthQuake: Earthquake! { get set }
}

protocol MapDetailDisplay: class {
    func setLocation(longitude: Double, latitude: Double)
    func setAnnotation(_ annotation: EarthquakeAnnotation)
}

final class MapDetailPresenter {
    
    // MARK: - Properties
    
    private unowned let display: MapDetailDisplay
    
    private let formatter: DateFormatter = DateFormatter()
    
    var earthQuake: Earthquake!
    
    // MARK: - Initialiser
    
    init(display: MapDetailDisplay) {
        self.display = display
    }
}

extension MapDetailPresenter: MapDetailPresenting {
    
    func viewDidBecomeReady() {
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        display.setLocation(longitude: earthQuake.longitude, latitude: earthQuake.latitude)
        display.setAnnotation(EarthquakeAnnotation(longitude: earthQuake.longitude,
                                                   latitude: earthQuake.latitude,
                                                   title: earthQuake.place,
                                                   subtitle: formatter.string(from: earthQuake.time)))
    }
}

