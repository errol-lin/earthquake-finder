//
//  MapListPresenter.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation

protocol MapListPresenting: class {
    func viewDidBecomeReady()
    var earthQuakes: [Earthquake] { get set }
}

protocol MapListDisplay: class {
    func setAnnotations(_ annotations: [EarthquakeAnnotation])
}

final class MapListPresenter {
    
    // MARK: - Properties
    
    private unowned let display: MapListDisplay
    
    private let formatter: DateFormatter = DateFormatter()
    
    var earthQuakes: [Earthquake] = []
    
    // MARK: - Initialiser
    
    init(display: MapListDisplay) {
        self.display = display
    }
}

extension MapListPresenter: MapListPresenting {
    
    func viewDidBecomeReady() {
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        let annotations: [EarthquakeAnnotation] = earthQuakes.map { (earthquake: Earthquake) -> EarthquakeAnnotation in
            EarthquakeAnnotation(longitude: earthquake.longitude,
                                 latitude: earthquake.latitude,
                                 title: earthquake.place,
                                 subtitle: formatter.string(from: earthquake.time))
        }
        display.setAnnotations(annotations)
    }
}
