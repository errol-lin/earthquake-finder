//
//  ListViewController.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import UIKit

final class ListViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.refreshControl = refreshControl
        }
    }
    
    @IBOutlet private var rightBarButton: UIBarButtonItem!
    
    // MARK: - Properties
    
    var presenter: ListPresenting!
    
    private var places: [String] = []
    
    private lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        return control
    }()
    
    // MARK: - Actions
    
    @IBAction private func rightBarButtonTapped(_ sender: Any) {
        presenter.didSelectList()
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.viewDidBecomeReady()
        presenter.fetchList()
    }
    
    // MARK: - Private Helpers
    
    @objc private func refresh() {
        presenter.fetchList()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let viewController as MapDetailViewController:
            presenter.prepareData(for: viewController.presenter)
        case let viewController as MapListViewController:
            presenter.prepareData(for: viewController.presenter)
        default:
            break
        }
    }
}

// MARK: - ListDisplay

extension ListViewController: ListDisplay {
    
    func setMapButton(title: String) {
        rightBarButton.title = title
    }
    
    func insert(earthquakeList: [String]) {
        places = earthquakeList
        tableView.reloadData()
    }
    
    func hideRefreshIndicator() {
        refreshControl.endRefreshing()
    }
}

// MARK: - ListRouter

extension ListViewController: ListRouter {
    
    func navigate(withIdentifier identifier: String) {
        performSegue(withIdentifier: identifier, sender: nil)
    }
}

// MARK: - UITableViewDataSource

extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "earthquakeCell")
        cell.textLabel?.text = places[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.didSelectEarthquake(index: indexPath.row)
    }
}
