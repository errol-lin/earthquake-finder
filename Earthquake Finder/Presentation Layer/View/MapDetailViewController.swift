//
//  MapDetailViewController.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import UIKit
import MapKit

final class MapDetailViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet private var mapView: MKMapView!
    
    // MARK: - Properties
    
    var presenter: MapDetailPresenting!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidBecomeReady()
    }
}

extension MapDetailViewController: MapDetailDisplay {
    
    func setLocation(longitude: Double, latitude: Double) {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 500000, longitudinalMeters: 500000)
        mapView.setRegion(region, animated: true)
    }
    
    func setAnnotation(_ annotation: EarthquakeAnnotation) {
        mapView.addAnnotation(annotation)
    }
}
