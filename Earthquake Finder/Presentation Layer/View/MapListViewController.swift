//
//  MapListViewController.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import UIKit
import MapKit

final class MapListViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private var mapView: MKMapView!
    
    // MARK: - Properties
    
    var presenter: MapListPresenting!
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidBecomeReady()
    }
}

extension MapListViewController: MapListDisplay {
    
    func setAnnotations(_ annotations: [EarthquakeAnnotation]) {
        mapView.addAnnotations(annotations)
    }
}

extension MapListViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? EarthquakeAnnotation else { return nil }
        let view: MKPinAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: "EarthquakeView") as? MKPinAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "EarthquakeView")
            view.canShowCallout = true
        }
        return view
    }
}
