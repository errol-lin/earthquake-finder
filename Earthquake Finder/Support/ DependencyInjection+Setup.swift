//
//   DependencyInjection+Setup.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import SwinjectStoryboard
import Swinject

extension SwinjectStoryboard {
    
    public static func setup() {
        
        defaultContainer.register(URLSessionProvider.self) { _ in
            URLSession.shared
        }
        
        defaultContainer.register(HTTPClientProvider.self) { (resolver: Resolver) in
            HTTPClient(urlSession: resolver.resolve(URLSessionProvider.self)!)
        }
        
        defaultContainer.register(EarthquakeMapping.self) { _ in
            EarthquakeMapper()
        }
        
        defaultContainer.register(EarthquakeRepository.self) { (resolver: Resolver) in
            EarthquakeService(client: resolver.resolve(HTTPClientProvider.self)!,
                        mapper: resolver.resolve(EarthquakeMapping.self)!)
        }
        
        defaultContainer.register(FetchEarthquakeInfoUseCase.self) { (resolver: Resolver) in
            FetchEarthquakeInfoUseCaseImplementation(repository: resolver.resolve(EarthquakeRepository.self)!)
        }
        
        defaultContainer.register(ListPresenting.self) { (resolver: Resolver, display: ListDisplay, router: ListRouter) in
            ListPresenter(display: display,
                          router: router,
                          earthquakeInfoUseCase: resolver.resolve(FetchEarthquakeInfoUseCase.self)!)
        }
        
        defaultContainer.storyboardInitCompleted(ListViewController.self) { (resolver: Resolver, controller: ListViewController) in
            controller.presenter = resolver.resolve(ListPresenting.self, arguments: controller as ListDisplay, controller as ListRouter)!
        }
        
        defaultContainer.register(MapDetailPresenting.self) { (resolver: Resolver, display: MapDetailDisplay) in
            MapDetailPresenter(display: display)
        }
        
        defaultContainer.storyboardInitCompleted(MapDetailViewController.self) { (resolver: Resolver, controller: MapDetailViewController) in
            controller.presenter = resolver.resolve(MapDetailPresenting.self, argument: controller as MapDetailDisplay)!
        }
        
        defaultContainer.register(MapListPresenting.self) { (resolver: Resolver, display: MapListDisplay) in
            MapListPresenter(display: display)
        }
        
        defaultContainer.storyboardInitCompleted(MapListViewController.self) { (resolver: Resolver, controller: MapListViewController) in
            controller.presenter = resolver.resolve(MapListPresenting.self, argument: controller as MapListDisplay)!
        }
    }
}
