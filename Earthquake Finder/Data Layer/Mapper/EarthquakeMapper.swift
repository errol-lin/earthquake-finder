//
//  EarthquakeMapper.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation

protocol EarthquakeMapping {
    /**
     Function that maps Data object to Domain object.
     - returns: Domain layer News object.
     */
    func map(response: EarthquakeResponse) throws -> EarthquakeInfo
}

final class EarthquakeMapper: EarthquakeMapping {
    
    // MARK: - EarthquakeMapping
    
    func map(response: EarthquakeResponse) throws -> EarthquakeInfo {
        let earthquakes: [Earthquake] = try response.features.compactMap { data -> Earthquake? in
            guard data.geometry.coordinates.count == 3 else { throw APIError.conversionError }
            return Earthquake(place: data.properties.place,
                              time: Date(timeIntervalSince1970: data.properties.time/1000),
                              longitude: data.geometry.coordinates[0],
                              latitude: data.geometry.coordinates[1])
        }
        
        let extraInfo: ExtraInfo = try map(bbox: response.bbox)
        
        return .init(earthquakes: earthquakes, extraInfo: extraInfo)
    }
    
    func map(bbox: [Double]) throws -> ExtraInfo {
        guard bbox.count == 6 else { throw APIError.conversionError }
        
        return ExtraInfo(minLongitude: bbox[0],
                         maxLongitude: bbox[3],
                         minLatitude: bbox[1],
                         maxLatitude: bbox[4])
    }
}
