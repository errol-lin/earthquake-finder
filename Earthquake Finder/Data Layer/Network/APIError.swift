//
//  APIError.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

/// Data layer errors that should not be exposed to Presentation layer.
enum APIError: Error {
    case authenticationFailed
    case notConnectedToInternet
    case unexpected
    case unexpectedResponseType
    case noDataReturned
    case conversionError
}
