//
//  HTTPClientProvider.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation
import RxSwift

protocol HTTPClientProvider {
    func singleDataTask<T>(with request: URLRequest) -> Single<T> where T: Decodable
}

final class HTTPClient: HTTPClientProvider {
    
    // MARK: - Properties
    
    private let urlSession: URLSessionProvider
    
    // MARK: - Initialiser
    
    init(urlSession: URLSessionProvider) {
        self.urlSession = urlSession
    }
    
    // MARK: - HTTPClientProvider
    
    func singleDataTask<T>(with request: URLRequest) -> Single<T> where T : Decodable {
        return Single<T>.create { single in
            let task = self.urlSession.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                self.handleTaskResult(with: data, response: response, error: error, single: single)
            }
            task.resume()
            return Disposables.create(with: task.cancel)
        }
    }
    
    // MARK: - Private Helpers
    
    private func handleTaskResult<T>(with data: Data?, response: URLResponse?, error: Error?, single: @escaping (SingleEvent<T>) -> Void) where T: Decodable {
        if let error = error {
            handleTaskError(error, single: single)
        } else {
            handleTaskResponse(response, data: data, single: single)
        }
    }
    
    private func handleTaskError<T>(_ error: Error, single: @escaping (SingleEvent<T>) -> Void) where T: Decodable {
        let networkError: NSError = error as NSError
        
        switch networkError.code {
        case NSURLErrorNotConnectedToInternet:
            single(.error(APIError.notConnectedToInternet))
        default:
            single(.error(APIError.unexpected))
        }
    }
    
    private func handleTaskResponse<T>(_ response: URLResponse?, data: Data?, single: @escaping (SingleEvent<T>) -> Void) where T: Decodable {
        guard let response = response as? HTTPURLResponse else {
            single(.error(APIError.unexpectedResponseType))
            return
        }
        
        // Check for success status code
        let httpSuccessCode: Range<Int> = 200 ..< 300
        if httpSuccessCode.contains(response.statusCode) {
            if let data = data, !data.isEmpty {
                decodeResponse(data: data, single: single)
            } else {
                single(.error(APIError.noDataReturned))
            }
            return
        }
        
        // Check for errors that come back in the response body.
        guard let errorCode: HTTPErrorCode = HTTPErrorCode(rawValue: response.statusCode) else {
            single(.error(APIError.unexpected))
            return
        }
        
        switch errorCode {
        case .authenticationFailed:
            single(.error(APIError.authenticationFailed))
        case .notFound:
            single(.error(APIError.noDataReturned))
        }
    }
    
    private func decodeResponse<T>(data: Data, single: @escaping (SingleEvent<T>) -> Void) where T: Decodable {
        do {
            let value = try JSONDecoder().decode(T.self, from: data)
            single(.success(value))
        } catch {
            single(.error(APIError.conversionError))
        }
    }
}

private enum HTTPErrorCode: Int {
    case authenticationFailed = 401
    case notFound = 404
}
