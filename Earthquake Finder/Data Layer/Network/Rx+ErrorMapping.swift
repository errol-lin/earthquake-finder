//
//  Rx+ErrorMapping.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import RxSwift

extension PrimitiveSequence {
    
    /// An extension function on Rx sequence that maps Data layer errors to Domain errors for better error handling.
    func mapError() -> RxSwift.PrimitiveSequence<Trait, Element> {
        return self.catchError { (error: Error) -> PrimitiveSequence<Trait, Element> in
            guard let apiError = error as? APIError else {
                throw RepositoryError.unexpected
            }
            
            switch apiError {
            case .notConnectedToInternet:
                throw RepositoryError.noNetwork
                
            case .authenticationFailed:
                throw RepositoryError.unauthorised
                
            case .conversionError, .unexpected, .unexpectedResponseType, .noDataReturned:
                throw RepositoryError.unexpected
            }
        }
    }
}
