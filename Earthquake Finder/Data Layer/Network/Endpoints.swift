//
//  Endpoints.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

struct Endpoints {
    
    static let earthquakeURL: String = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson"
}
