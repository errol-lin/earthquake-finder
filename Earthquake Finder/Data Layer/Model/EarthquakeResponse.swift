//
//  EarthquakeResponse.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation

struct EarthquakeResponse: Decodable {
    let features: [EarthquakeInfoData]
    let bbox: [Double]
    
    struct EarthquakeInfoData: Decodable {
        let properties: Property
        let geometry: Geometry
        
        struct Property: Decodable {
            let place: String
            let time: TimeInterval
        }
        
        struct Geometry: Decodable {
            let coordinates: [Double]
        }
    }
}
