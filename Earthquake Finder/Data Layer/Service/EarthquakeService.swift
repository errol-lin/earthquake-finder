//
//  EarthquakeService.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation
import RxSwift

final class EarthquakeService: EarthquakeRepository {
    
    // MARK: - Properties
    
    private let client: HTTPClientProvider
    
    private let mapper: EarthquakeMapping
    
    // MARK: - Initialiser
    
    init(client: HTTPClientProvider, mapper: EarthquakeMapping) {
        self.client = client
        self.mapper = mapper
    }
    
    // MARK: - EarthquakeRepository
    
    func fetchEarthquakeInfo() -> Single<EarthquakeInfo> {
        let url = URL(string: Endpoints.earthquakeURL)!
        let request = URLRequest(url: url)
        
        return (client.singleDataTask(with: request) as Single<EarthquakeResponse>)
            .map(mapper.map)
            .mapError()
            .observeOn(MainScheduler.instance)
    }
}
