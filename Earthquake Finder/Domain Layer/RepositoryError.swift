//
//  RepositoryError.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

/// Domain layer errors that will be handled in Presentation layer.
enum RepositoryError: Error {
    case unauthorised
    case noNetwork
    case unexpected
}
