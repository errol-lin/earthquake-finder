//
//  EarthquakeRepository.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import RxSwift

protocol EarthquakeRepository {
    func fetchEarthquakeInfo() -> Single<EarthquakeInfo>
}
