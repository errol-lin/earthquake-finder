//
//  FetchEarthquakeInfoUseCase.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import RxSwift

protocol FetchEarthquakeInfoUseCase {
    func fetchInfo() -> Single<EarthquakeInfo>
}

final class FetchEarthquakeInfoUseCaseImplementation: FetchEarthquakeInfoUseCase {
    
    // MARK - Properties
    
    private let repository: EarthquakeRepository
    
    // MARK: - Initialiser
    
    init(repository: EarthquakeRepository) {
        self.repository = repository
    }
    
    // MARK: - FetchEarthquakeInfoUseCase
    
    func fetchInfo() -> Single<EarthquakeInfo> {
        return repository.fetchEarthquakeInfo()
    }
}
