//
//  EarthquakeInfo.swift
//  Earthquake Finder
//
//  Created by Zehua(Errol) Lin on 13/11/18.
//  Copyright © 2018 Zehua(Errol) Lin. All rights reserved.
//

import Foundation

struct EarthquakeInfo {
    let earthquakes: [Earthquake]
    let extraInfo: ExtraInfo
}

struct Earthquake {
    let place: String
    let time: Date
    let longitude: Double
    let latitude: Double
}

struct ExtraInfo {
    let minLongitude: Double
    let maxLongitude: Double
    let minLatitude: Double
    let maxLatitude: Double
}
